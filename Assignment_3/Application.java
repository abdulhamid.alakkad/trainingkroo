import java.util.ArrayList;
import java.util.Scanner;

public class Application {

  public static void main(String[] args) {
    // EuroDenomination euroDenomination;
    System.out.println("value of 1_CENT_COINS = " + EuroDenomination.CENT_COINS_1.getCentValue());
    System.out.println("Payment instrument of 1_CENT_COINS = " + EuroDenomination.CENT_COINS_1.getPaymentInstrument());

    ArrayList<EuroDenomination> euroDenomination = new ArrayList<EuroDenomination>();
    euroDenomination.add(0, EuroDenomination.CENT_COINS_1);
    euroDenomination.add(1, EuroDenomination.CENT_COINS_2);
    euroDenomination.add(2, EuroDenomination.CENT_COINS_5);

    System.out.println("sum = " + EuroCalculator.sumCents(euroDenomination));

    ArrayList<EuroDenomination> euroDenominationTest = new ArrayList<EuroDenomination>();
    euroDenominationTest = EuroCalculator.makePaymentInstrument(38233);
    for (int i = 0; i < euroDenominationTest.size(); i++) {
      System.out.println(euroDenominationTest.get(i));
    }

    System.out.println("...........................");

    ArrayList<EuroDenomination> euroDenominationTest2 = new ArrayList<EuroDenomination>();
    euroDenominationTest2 = EuroCalculator.euroCalculate(euroDenominationTest, 20000);
    for (int i = 0; i < euroDenominationTest2.size(); i++) {
      System.out.println(euroDenominationTest2.get(i));
    }
    System.out.println("...........................");

    ArrayList<Product> products = new ArrayList<Product>();
    products.add(0, new Product("Latte", 30000));
    products.add(1, new Product("Cappuccino", 45000));
    products.add(2, new Product("Caramel Macchiato", 65250));
    products.add(3, new Product("White Chocolate Mocha", 66890));
    products.add(4, new Product("Pike Place Roast", 77010));

    // adapting
    String[] stringProducts = new String[products.size()];
    for (int i = 0; i < products.size(); i++) {
      stringProducts[i] = products.get(i).getProduct();
      System.out.println("product " + i + ": " + stringProducts[i]);
    }

    // from here start the interface with user
    System.out.println("...........................");
    System.out.println("          CAFE             ");
    System.out.println("...........................");

    String msg = "Hallo!.. you can choose your drink!";
    String choosen = ConsoleInput.getChoice(stringProducts, msg);
    System.out.println(choosen);

    // get its price
    // if statement with string comparison -> you have to use A.equals()
    int cost = 0;
    for (int i = 0; i < products.size(); i++) {
      if (choosen.equals(products.get(i).getProduct())) {
        cost = products.get(i).getPrice();
        System.out.println("your drink costs " + cost + " cent");
      }
    }

    System.out.println("please enter your cash in cent!");
    Scanner scan = new Scanner(System.in);
    int given = scan.nextInt();
    // System.out.println(a);

    ArrayList<EuroDenomination> givenPayment = new ArrayList<EuroDenomination>();
    givenPayment = EuroCalculator.makePaymentInstrument(given);

    ArrayList<EuroDenomination> calculateToBack = new ArrayList<EuroDenomination>();
    calculateToBack = EuroCalculator.euroCalculate(givenPayment, cost);

    System.out.println("to pay back");
    for (int i = 0; i < calculateToBack.size(); i++) {
      System.out.println(calculateToBack.get(i).getPaymentInstrument());
    }
  }
}
