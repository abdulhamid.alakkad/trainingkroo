public enum EuroDenominationStrings{
  1_CENT_COINS("00,01"),
  2_CENT_COINS("00,02"),
  5_CENT_COINS("00,05"),
  10_CENT_COINS("00,10"),
  20_CENT_COINS("00,20"),
  50_CENT_COINS("00,50"),
  1_EURO_COINS("01,00"),
  2_EURO_COINS("02,00"),
  5_EURO_BILL("05,00"),
  10_EURO_BILL("10,00"),
  20_EURO_BILL("20,00"),
  50_EURO_BILL("50,00"),
  100_EURO_BILL("100,00"),
  200_EURO_BILL("200,00");

  private String PaymentInstrument;

  public String getPaymentInstrument(){
    return PaymentInstrument;
  }
}
