public enum EuroDenomination {
  EURO_BILL_200(20000, "200,00"), //
  EURO_BILL_100(10000, "100,00"), //
  EURO_BILL_50(5000, "50,00"), //
  EURO_BILL_20(2000, "20,00"), //
  EURO_BILL_10(1000, "10,00"), //
  EURO_BILL_5(500, "05,00"), //
  EURO_COINS_2(200, "02,00"), //
  EURO_COINS_1(100, "01,00"), //
  CENT_COINS_50(50, "00,50"), //
  CENT_COINS_20(20, "00,20"), //
  CENT_COINS_10(10, "00,10"), //
  CENT_COINS_5(5, "00,05"), //
  CENT_COINS_2(2, "00,02"), //
  CENT_COINS_1(1, "00,01"); //

  private int centValue;
  private String paymentInstrument;

  // you have to make a constructor
  EuroDenomination(int centValue, String paymentInstrument) {
    this.centValue = centValue;
    this.paymentInstrument = paymentInstrument;
  }

  public int getCentValue() {
    return centValue;
  }

  public String getPaymentInstrument() {
    return paymentInstrument;
  }
}

// EuroDenominationStrings.1_CENT_COINS.name() // value()
// https://stackoverflow.com/questions/8811815/is-it-possible-to-assign-numeric-value-to-an-enum-in-java
