import java.util.ArrayList;

public class EuroCalculator {

  // help function
  // get sum of cents
  public static int sumCents(ArrayList<EuroDenomination> euroDenomination) {
    int sum = 0;
    for (int i = 0; i < euroDenomination.size(); i++) {
      sum += euroDenomination.get(i).getCentValue();
    }
    return sum;
  }

  // get payment instrument from cents
  public static ArrayList<EuroDenomination> makePaymentInstrument(int cents) {
    ArrayList<EuroDenomination> euroDenomination = new ArrayList<EuroDenomination>();
    int index = 0;
    while (cents > 0) {

      for (EuroDenomination value : EuroDenomination.values()) {
        if (cents >= value.getCentValue()) {
          euroDenomination.add(index, value);
          cents = cents - value.getCentValue();
          index++;
        }
      }
    }
    return euroDenomination;
  }

  // purchase to buy = einkauf
  public static ArrayList<EuroDenomination> euroCalculate(ArrayList<EuroDenomination> givenPayment,
      int purchaseInCent) {
    ArrayList<EuroDenomination> changeToback = new ArrayList<EuroDenomination>();
    int givenPaymentCents = sumCents(givenPayment);
    if (givenPaymentCents < purchaseInCent) {
      System.out.println("your payment is not enough");
      System.exit(0);
    } else {
      int changeTobackCents = givenPaymentCents - purchaseInCent;
      changeToback = makePaymentInstrument(changeTobackCents);
    }
    return changeToback;
  }
}
