public class Product{
  String product;
  int price;

    public Product(String product, int price){
    this.product = product;
    this.price = price;
  }
  
  public String getProduct(){
    return this.product;
  }

  public int getPrice(){
    return this.price;
  }

  public void setProduct(String newProduct){
    this.product = newProduct;
  }

  public void setPrice(int newPrice){
    this.price = newPrice;
  }
}
