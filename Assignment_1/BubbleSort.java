import java.util.Arrays;
import java.util.Random;
public class BubbleSort{

  public static String arrayToString(int[] arr){
  String str = Arrays.toString(arr);
  return str;
  }

  public static int[] randomArray(int n){
    Random r = new Random();
    int [] randomArray = new int[n];
    for (int i = 0; i < n; i++){
      randomArray[i] = r.nextInt(99);
    }
    return randomArray;
  }

  public static int [] bubbleSort(int[] arr){
    int temp;
    for (int i = 1; i < arr.length; i++){
      for (int j = 0; j < arr.length - i; j++){
        if (arr [j] > arr [j + 1]){
           temp = arr [j];
           arr[j] = arr [j + 1];
           arr [j +1] = temp;
          }
        }
      }
      return arr;
    }

  public static void main (String[] args){
  int [] bsp ={1, 2, 3, 4};
  System.out.println(arrayToString(bsp));
  System.out.println(arrayToString(randomArray(9)));

  int[] A = randomArray(5);
  int[] B = randomArray(6);
  int[] C = randomArray(7);
  System.out.println(arrayToString(bubbleSort(A)));
  System.out.println(arrayToString(bubbleSort(B)));
  System.out.println(arrayToString(bubbleSort(C)));
  }

}
