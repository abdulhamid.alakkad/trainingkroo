import java.util.Arrays;
public class Primes {

  public static String arrayToString(int[] arr){
    String str = Arrays.toString(arr);
    return str;
  }

  public static int[] fillArray(int n){
    int[] arrayTofill = new int[n - 1];
    int startValue = 2;
    for(int i = 0; i < arrayTofill.length; i++){
      arrayTofill[i] = startValue;
      startValue++;
    }
    return arrayTofill;
  }

  public static void filterArray(int[] arr, int index){
    int element = arr[index];
    for (int i = index + 1; i < arr.length; i++){
      if(arr[i] % element == 0){
        arr[i] = -1;
      }
    }
  }

  public static int[] removeElement(int[] array, int elementToremove){
    int index = 0;
    for(int i = 0; i < array.length; i++){
      if(array[i] != elementToremove){
        array[index++] = array[i];
      }
    }
    return Arrays.copyOf(array, index);
  }

  public static void primes(int n){
    int[] primesArray = fillArray(n);
    filterArray(primesArray, 0);
    filterArray(primesArray, 1);
    filterArray(primesArray, 3);
    filterArray(primesArray, 5);
    String StringPrimesArray = arrayToString(removeElement(primesArray, -1));
    System.out.println(StringPrimesArray);
  }





  public static void main (String[] args){
    //System.out.println(arrayToString(fillArray(10)));
    int[] array = fillArray(15);
    filterArray(array, 0);
    System.out.println(arrayToString(array));
    filterArray(array, 1);
    System.out.println(arrayToString(removeElement(array, -1)));
    primes(15);
    primes(50);

  }
}
