package de.uni_hannover.hci.alakkad.model;

public class Clover {

  private int value;

  public Clover(int value) {
    this.value = value;
  }

  public int getValue() {
    return this.value;
  }

  public static String str() {
    return "O";
  }
}
