package de.uni_hannover.hci.alakkad.model;

public class Ladybug {

  private Clover[] clovers;
  public Direction direction;

  public Ladybug(Direction newDirection) {
    this.direction = newDirection;
    this.clovers = new Clover[10];
  }

  public Ladybug() {
    this(Direction.NORTH);
  }

  public Direction getDirection() {
    return this.direction;
  }

  public void setDirection(Direction newDirection) {
    this.direction = newDirection;
  }

  public void turnRight() {
    this.direction = this.direction.Right();
  }

  public void turnLeft() {
    this.direction = this.direction.Left();
  }

  // sumCloverValues or getCloversSum
  public int getCloverValues() {
    int result = 0;
    for (int i = 0; i < this.clovers.length; i++) {
      result += this.clovers[i].getValue();
    }
    return result;
  }
}
