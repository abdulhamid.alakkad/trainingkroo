package de.uni_hannover.hci.alakkad.model;

public enum Direction {
    NORTH("N"), EAST("E"), SOUTH("S"), WEST("W");

    private String value;

    Direction(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    Direction Left() {
        switch (this) {
            case NORTH:
                return Direction.WEST;
            case EAST:
                return Direction.NORTH;
            case SOUTH:
                return Direction.EAST;
            case WEST:
                return Direction.SOUTH;
            default:
                return null;
        }
    }

    Direction Right() {
        switch (this) {
            case NORTH:
                return Direction.EAST;
            case EAST:
                return Direction.SOUTH;
            case SOUTH:
                return Direction.WEST;
            case WEST:
                return Direction.NORTH;
            default:
                return null;
        }
    }
}
