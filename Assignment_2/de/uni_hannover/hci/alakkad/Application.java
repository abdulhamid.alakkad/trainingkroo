package de.uni_hannover.hci.alakkad;

import java.util.Arrays;

import de.uni_hannover.hci.alakkad.model.Direction;

public class Application {

  public static void main(String[] args) {

    int[][] initialPlayingBoard = { { 1, 4, 1, 1, 1, 1, 1, 1, 1, 1 }, { 1, 2, 0, 0, 1, 0, 0, 0, 0, 1 },
        { 1, 0, 1, 1, 1, 0, 1, 1, 0, 1 }, { 1, 0, 0, 1, 0, 0, 1, 0, 0, 1 }, { 1, 1, 0, 0, 0, 1, 1, 1, 0, 1 },
        { 1, 0, 0, 1, 0, 1, 0, 0, 0, 1 }, { 1, 1, 1, 1, 1, 1, 3, 1, 1, 1 } };

    Game game = new Game(initialPlayingBoard, Direction.SOUTH);

    System.out.println(Arrays.toString(args));

    if (args == null || args.length == 0) {
      System.out.println("Choose either algorithm or interactive");
      return;
    }

    if (args[0].equals("algorithm")) {
      play(game);
    } else if (args[0].equals("interactive")) {
      playInteractively(game, args);
    } else {
      System.out.println("I can't play mother fucker");
    }
  }

  private static void play(Game game) {
    // TODO: implement the algorithm
      game.playAlgorithm();
  }

  private static void playInteractively(Game game, String[] args) {
    // TODO: play with user
    System.out.println("Hallo in kara game");
    System.out.println("each step you have to select a command from the available options");
    game.display();
    System.out.println("select a command [move forward, turn right or turn left]");
    game.playUser();
  }
}
