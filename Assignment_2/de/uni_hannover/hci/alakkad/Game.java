package de.uni_hannover.hci.alakkad;

import de.uni_hannover.hci.alakkad.model.*;

import java.util.Scanner;

public class Game {

  private final static String EMPTY_FIELD = ".";

  private Ladybug player;
  private Clover clover = new Clover(10);
  private String[][] playingBoard;
  private int[] currentBugPosition;
  private int points = 0;

  public Game(int[][] intArray, Direction direction) {
    this.player = new Ladybug(direction);
    initGame(intArray);
  }

  private void initGame(int[][] intArray) {
    this.playingBoard = new String[intArray.length][intArray[0].length];
    this.currentBugPosition = new int[2];

    for (int i = 0; i < intArray.length; i++) {
      for (int j = 0; j < intArray[i].length; j++) {
        if (intArray[i][j] == 1) {
          this.playingBoard[i][j] = Tree.str();
        } else if (intArray[i][j] == 2) {
          this.playingBoard[i][j] = Clover.str();
        } else if (intArray[i][j] == 3) {
          this.playingBoard[i][j] = "Z";
        } else if (intArray[i][j] == 4) {
          this.playingBoard[i][j] = this.player.getDirection().getValue();
          this.currentBugPosition[0] = i;
          this.currentBugPosition[1] = j;
        } else {
          this.playingBoard[i][j] = ".";
        }
      }
    }
  }

  // return coordinates of the new position after moving forward, right or left
  private int[] determinePosition() {

    switch (this.player.direction) {
      case SOUTH:
        return new int[] { this.currentBugPosition[0] + 1, this.currentBugPosition[1] };
      case NORTH:
        return new int[] { this.currentBugPosition[0] - 1, this.currentBugPosition[1] };
      case EAST:
        return new int[] { this.currentBugPosition[0], this.currentBugPosition[1] + 1 };
      case WEST:
        return new int[] { this.currentBugPosition[0], this.currentBugPosition[1] - 1 };
    }
    return new int[2];
  }

  // cheack if the ladybug can move
  private boolean canMove(int[] newPosition) {

    if (this.playingBoard[newPosition[0]][newPosition[1]].equals(Tree.str())) {
      return false;
    }

    return true;
  }

  public boolean forward() {
    int[] newPosition = determinePosition();

    if (!canMove(newPosition)) {
      return false;
    }

    if (atGoal(newPosition)) {
      System.exit(0);
    }

    if (atClover(newPosition)) {
      points += clover.getValue();
    }

    this.playingBoard[this.currentBugPosition[0]][this.currentBugPosition[1]] = EMPTY_FIELD;
    this.playingBoard[newPosition[0]][newPosition[1]] = this.player.getDirection().getValue();
    this.currentBugPosition = newPosition;
    return true;
  }

  public boolean atGoal(int[] Position) {
    if (this.playingBoard[Position[0]][Position[1]].equals("Z")) {
      System.out.println("you have won the game!");
      return true;
    }
    return false;
  }

  public boolean atClover(int[] Position) {
    if (this.playingBoard[Position[0]][Position[1]].equals(clover.str())) {
      return true;
    }
    return false;
  }

  public void right() {
    this.player.turnRight();
    this.playingBoard[this.currentBugPosition[0]][this.currentBugPosition[1]] = this.player.getDirection().getValue();
  }

  public void left() {
    this.player.turnLeft();
    this.playingBoard[this.currentBugPosition[0]][this.currentBugPosition[1]] = this.player.getDirection().getValue();
  }

  public void playAlgorithm() {
    // cheack right
    right();
    if (forward()) {
      display();
      playAlgorithm();
    } else {
      // cheack forward
      left();
      if (forward()) {
        display();
        playAlgorithm();
      } else {
        // cheack left
        left();
        if (forward()) {
          display();
          playAlgorithm();
        } else {
          // cheack backword
          left();
          if (forward()) {
            display();
            playAlgorithm();
          }
        }
      }
    }
  }

  public void playUser() {
    Scanner scan = new Scanner(System.in);
    String userInput = scan.nextLine();

    switch (userInput) {
      case "move forward":
        if (forward()) {
          ;
          display();
          System.out.println("select a command [move forward, turn right or turn left]");
          playUser();
        } else {
          System.out.println("you can not move!..please turn right or left!");
          playUser();
        }
        break;
      case "turn right":
        right();
        display();
        System.out.println("select a command [move forward, turn right or turn left]");
        playUser();
        break;
      case "turn left":
        left();
        display();
        System.out.println("select a command [move forward, turn right or turn left]");
        playUser();
        break;
      default:
        System.out.println("wrong choose!!!! please choose one of the available commands!");
        playUser();
    }
  }

  public void display() {
    System.out.print("  ");
    for (int i = 0; i < this.playingBoard[0].length; i++) {
      System.out.print(i + " ");
    }
    System.out.println();
    for (int i = 0; i < this.playingBoard.length; i++) {
      for (int j = 0; j < this.playingBoard[0].length; j++) {
        if (j == 0) {
          System.out.print(i + " ");
        }
        System.out.print(this.playingBoard[i][j] + " ");
      }
      System.out.println();
    }
    System.out.println("points: " + points);
    System.out.println();
  }
}
